import React, { useEffect, useState } from 'react';
import Graph from "./component/Graph";
import './App.css';


function App() {

  
  return (
    <div className="App">
      <div className="wrapper">
        
        <Graph/>
        
      </div>
    </div>
  );
}

export default App;
