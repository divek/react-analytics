
import React, { useState, useEffect } from "react";
import { Line } from "react-chartjs-2";
import axios from "axios";


const graphDataUrl = "https://simpleanalytics.com/simpleanalytics.com.json?version=4&fields=histogram";
  
const Graph = () => {

    const [chartData, setChartData] = useState({});
    const [userData, setUserData] = useState({});

    const chart = () => {

        let date = [];
        let pageviews = [];
        let hostName = [];

        axios

        .get("https://simpleanalytics.com/simpleanalytics.com.json?version=4&fields=histogram")
        .then(res => {
            // console.log("dddddd", res.data.agents);
            
            for (const dataObj of res.data.histogram) {
                date.push(parseInt(dataObj.date));
                pageviews.push(parseInt(dataObj.pageviews));
                hostName.push(parseInt(dataObj).hostname)
            }
            setChartData({
                labels: date,
                datasets: [
                    {
                    label: "",
                    data: pageviews,
                    backgroundColor: ["rgba(75, 192, 192, 0.6)"],
                    borderWidth: 2
                    }
                ]
            });
        })
        .catch(err => {
            console.log(err);
        });
        // console.log("browser details", date, pageviews); 
    };

    useEffect(() => {
        chart();
        getGraphData();
    }, []);
  
  
     const getGraphData = async () => {
        const response = await fetch(graphDataUrl);
        const jsonData = await response.json();
        setUserData(jsonData);
      };

      
    return (
      <div className="grapWrap">
            {/* {console.log("dgegey", userData.hostname)} */}
             <h1 >Analytics for {userData.hostname}</h1>
             <p>website url : {userData.url}</p>
            <Line data={chartData} />
      </div>
    )

};

export default Graph; 